#!/usr/bin/python3
# -*- coding: utf-8 -*-

import datetime
import json
import sys
import os
# import io

import argparse

def loadf(path):
    with open(path) as _fjson:
        return json.loads(_fjson.read())

def mstos(milliseconds:int):
    dtime = datetime.timedelta(milliseconds=milliseconds)
    # take only 3 (from milliseconds) digits
    # and convert "." (dot) to "," (comma)
    dtime = str(dtime)[:-3].replace('.', ',')
    return f'{dtime:0>12}'

def join(raw:list):
    result = ''
    r = [dsub for dsub in raw.copy()]
    rLen = len(r)

    for count, item in enumerate(r, 1):
        item['startTime'], item['endTime'] = map(mstos, (item['startTime'], item['endTime']))
        if count == rLen: break
    for line, sub, in enumerate(r, 1):
        # ASK: use ''.join instead?
        result += f'{line}\n{sub["startTime"]} --> {sub["endTime"]}\n{sub["text"]}\n\n\n'

    return result

# def joinline(raw:list, line:int):
#     r = raw[line+1].copy()
#     r['startTime'], r['endTime'] = map(mstos, (r['startTime'], r['endTime']))

#     return f'{line+1}\n{r["startTime"]} --> {r["endTime"]}\n{r["text"]}\n\n\n'

def jsontosrt(fpath, ignore_empty=True, debug=False): # debug save for future implementation
    files, dirpath = fpath

    for _file in files:
        # with loadf(os.path.join(dirpath, _file)) as data:
        #     sub = io.FileIO(os.path.join(dirpath, os.path.splitext(file)[0], ext), 'w+')
        #     sub.write(join(data))
        #     print(f'[!] {_file} successfully converted')
        data = loadf(os.path.join(dirpath, _file))
        dest = ''.join((os.path.splitext(_file)[0].replace('-transcript', ''), '.srt'))
        dest = dest.replace(os.path.sep, '')

        sub = open(os.path.join(dirpath, dest), 'w')
        sub.write(join(data))
        print(f' > {_file}  🗸')
        sub.close()
    return

def psanit(path, ignore_empty=False):
    files = []
    if os.path.isdir(path):
        files.extend((f for f in os.listdir(path) if os.path.splitext(f)[1] == '.json'))
        dirpath = path
    elif os.path.isfile(path):
        files.append(os.path.basename(path))
        dirpath = os.path.dirname(path)

    if not ignore_empty:
        for _file in files:
            if os.path.getsize(os.path.join(dirpath, _file)) <= 2: sys.exit(1)

    # NOTE: I separate this due to speed issue
    for _file in files:
        if os.path.getsize(os.path.join(dirpath, _file)) <= 2:
            files.remove(_file)
            print(f'{_file} is empty', file=sys.stderr)

    return (files, dirpath)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', nargs='+', type=str, dest='d', metavar='directory', help='path to subtitles directory')
    parser.add_argument('-f', '--files', nargs='+', type=str, dest='f', metavar='file name', help='list of file names`')
    parser.add_argument('-e', '--ignore-empty', action='store_true', dest='e', help='ignore empty files (default action is immediately exit when empty files found')
    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    args = parser.parse_args()

    paths = []

    if args.d is not None:
        for sdir in args.d:
            paths.append(psanit(sdir, args.e))
    if args.f is not None:
        for fpath in args.f:
            paths.append(psanit(fpath, args.e))

    for _path in paths:
        jsontosrt(_path)
    sys.exit(0)

if __name__ == '__main__':
    main()
